let mongoose = require("mongoose");
let {
  transaksi
} = require('../models');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index')
let should = chai.should();
let transaksi_id;

chai.use(chaiHttp);

// ini nggak harus diremove dulu
describe('Transaksi', () => {
  beforeEach((done) => {
    transaksi.remove({}, (err) => {
      done()
    });
  });

  /*
   * Test the /GET route
   */
  describe('/GET transaksi', () => {
    it('it should GET all the transaksi', (done) => {
      chai.request(server) // request to server (index.js)
        .get('/transaksi')
        .end((err, res) => {
          res.should.have.status(200); // Response should have status 200
          res.body.should.be.an('object'); // Body Response should be an object
          res.body.should.have.property('status'); // Body Response should have 'status' property
          res.body.should.have.property('data'); // Body Response should have 'data' property
          res.body.data.should.be.an('array'); // Body Response .data should be an array
          done();
        });
    });
  });

  /*
   * Test the /POST route
   */
  describe('/POST transaksi', () => {
    it('it should POST a transaksi', (done) => {
      chai.request(server)
        .post('/transaksi/create')
        .send({
          id_barang: '5fccb45e683964d75bd4d3bc',
          id_pelanggan: '5fccb3f6683964d75bd4d3b6',
          jumlah: 20
        })
        .end((err, res) => {
          res.should.have.status(200); // Response should have status 200
          res.body.should.be.an('object'); // Body Response should be an object
          res.body.should.have.property('status'); // Body Response should have 'status' property
          res.body.should.have.property('data'); // Body Response should have 'data' property
          res.body.data.should.be.an('object'); // Body Response .data should be an array
          res.body.data.should.have.property('_id'); // data {_id: ....}
          done()
        });
    });
  });

  describe('/POST transaksi', () => {
    it('it should get one transaksi', (done) => {
      chai.request(server)
        .post('/transaksi/create')
        .send({
          id_barang: '5fccb45e683964d75bd4d3bc',
          id_pelanggan: '5fccb3f6683964d75bd4d3b6',
          jumlah: 20
        })
        .end((err, res) => {
          transaksi_id = res.body.data._id
          chai.request(server)
          .get(`/transaksi/${transaksi_id}`)
          .end((err, res) => {
            res.should.have.status(200); // Response should have status 200
            res.body.should.be.an('object'); // Body Response should be an object
            res.body.should.have.property('status'); // Body Response should have 'status' property
            res.body.should.have.property('data'); // Body Response should have 'data' property
            res.body.data.should.be.an('object'); // Body Response .data should be an array
            done()
          })
        });
    });
  });

});
